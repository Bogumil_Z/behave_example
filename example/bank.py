class Account(object):
    def __init__(self, balance):
        self._balance = balance

    def sub_money(self,value):
        self._balance -= value

    def add_money(self,value):
        self._balance += value

    def get_balance(self):
        return self._balance