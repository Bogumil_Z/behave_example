Feature: showing off behave

    Scenario: run a simple test
        Given: we have behave installed
        When: we implement a test
        Then behave will test it for us!

    Scenario: Counting number of users in departments
        Given a set of specific users
            | name      | department  |
            | Barry     | Beer Cans   |
            | Pudey     | Silly Walks |
            | Two-Lumps | Silly Walks |
        When we count the number of people in each department
        Then we will find two people in "Silly Walks"
        But we will find one person in "Beer Cans"
