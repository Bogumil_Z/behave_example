from behave import *
from collections import defaultdict

@given('we have behave installed')
def step_impl(context):
    pass

@when('we implement a test')
def step_impl(context):
    assert True is not False

@then('behave will test it for us!')
def step_impl(context):
    assert context.failed is False

@given('a set of specific users')
def step_impl(context):
    context.users = defaultdict(list)

    for row in context.table:
        context.users[row['department']].append(row['name'])

@when('we count the number of people in each department')
def step_impl(context):
    context.silly_count = len(context.users["Silly Walks"])
    context.beer_count = len(context.users["Beer Cans"])

@then('we will find two people in "Silly Walks"')
def step_impl(context):
    assert context.silly_count == 2

@then('we will find one person in "Beer Cans"')
def step_impl(context):
    assert context.beer_count ==1
