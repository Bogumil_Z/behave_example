from behave import *

from example.bank import Account


@given('my Current account has a balance of {balance}')
def step_impl(context, balance):
    context.current_account = Account(float(balance))


@step("my Savings account has a balance of {balance}")
def step_impl(context, balance):
    context.saving_account = Account(float(balance))


@when("I transfer {value} from my Current account to my Savings account")
def step_impl(context, value):
    context.current_account.sub_money(float(value))
    context.saving_account.add_money(float(value))


@then("I should have {value} in my Current account")
def step_impl(context, value):
    current_balance = context.current_account.get_balance()
    expected_balance = float(value)
    assert current_balance == expected_balance, "Not expected balance at " \
                                                "Saving account. Current balance is '{0}', " \
                                                "but expected '{1}'".format(current_balance,
                                                                            expected_balance)


@step("I should have {value} in my Savings account")
def step_impl(context, value):
    current_balance = context.saving_account.get_balance()
    expected_balance = float(value)
    assert current_balance == expected_balance, "Not expected balance at " \
                                                "Saving account. Current balance is '{0}', " \
                                                "but expected '{1}'".format(current_balance,
                                                                            expected_balance)
